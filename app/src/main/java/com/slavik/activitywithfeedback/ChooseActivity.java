package com.slavik.activitywithfeedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChooseActivity extends AppCompatActivity {

    public static final String YOUR_SIDE = "com.slavik.activitywithfeedback.your_side";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Button whiteButton = findViewById(R.id.whiteSideButton);
        Button darkButton = findViewById(R.id.darkSideButton);

        whiteButton.setOnClickListener(new WhiteButtonListener());
        darkButton.setOnClickListener(new DarkButtonListener());
    }

    class WhiteButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String WHITE_SIDE = "White side";

            Intent data = createIntent(WHITE_SIDE);
            setResult(RESULT_OK, data);
        }
    }

    class DarkButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final String DARK_SIDE = "Dark side";

            Intent data = createIntent(DARK_SIDE);
            setResult(RESULT_OK, data);
        }
    }


    private Intent createIntent(String message){
        Intent intent = new Intent(ChooseActivity.this, MainActivity.class);
        intent.putExtra(YOUR_SIDE,message);

        return intent;
    }

}


