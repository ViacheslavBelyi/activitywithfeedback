package com.slavik.activitywithfeedback;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView sideTv;
    private static final int FEEDBACK_FROM_CHOOSE_ACTIVITY = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sideTv = findViewById(R.id.sideTv);

        Button chooseButton = findViewById(R.id.chooseButton);
        chooseButton.setOnClickListener(new ChooseButtonListener());
    }

    class ChooseButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ChooseActivity.class);
            startActivityForResult(intent,FEEDBACK_FROM_CHOOSE_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if( requestCode == FEEDBACK_FROM_CHOOSE_ACTIVITY) {
            if (data != null) {
                sideTv.setText(data.getStringExtra(ChooseActivity.YOUR_SIDE));
            }
        }
    }


}
